function format_table() {
    var md_table_holder = document.getElementById('md_table_holder');
    var mtf = new MarkdownTableFormatter();
    mtf.format_table(md_table_holder.value);
    md_table_holder.value = mtf.output_table;
  }
  
  function loadExample(example_number) {
    var md_table_holder = document.getElementById('md_table_holder');
    switch (example_number) {
      case 1:
        md_table_holder.value = "| h1 | h2 | h3 |\n|-|-|-|\n| data1 | data2 | data3 |";
        break;
      case 2:
        md_table_holder.value = "h1 | h2 | h3\n-|-|-\ndata-1 | data-2 | data-3";
        break;
      case 3:
        md_table_holder.value = "| Header 1 | Header 2 | Header 3 |\n|----|---|-|\n| data1a | Data is longer than header | 1 |\n| d1b | add a cell|\n|lorem|ipsum|3|\n| | empty outside cells\n| skip| | 5 |\n| six | Morbi purus | 6 |";
    }
  }
  