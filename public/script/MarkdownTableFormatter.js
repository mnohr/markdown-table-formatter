function MarkdownTableFormatter() {
  // Setup instance variables.
  this.cells = [];
  this.column_widths = [];
  this.output_table = "";
}

////////////////////////////////////////////////////////////////////////////////

MarkdownTableFormatter.prototype.add_missing_cell_columns = function () {
  this.cells.forEach((row, rowIndex) => {
    this.column_widths.forEach((_, colIndex) => {
      if (typeof row[colIndex] === 'undefined') {
        row[colIndex] = '';
      }
    });
  });
};

////////////////////////////////////////////////////////////////////////////////

MarkdownTableFormatter.prototype.format_table = function (table) {
  this.import_table(table);
  this.get_column_widths();
  this.add_missing_cell_columns();
  this.pad_cells_for_output();

  // Header
  this.output_table = `| ${this.cells[0].join(" | ")} |\n`;

  // Separator 
  this.output_table += `|-${this.cells[1].join("-|-")}-|\n`;

  for (let i = 2; i < this.cells.length; i++) {
    this.output_table += `| ${this.cells[i].join(" | ")} |\n`;
  }
};

////////////////////////////////////////////////////////////////////////////////

MarkdownTableFormatter.prototype.get_column_widths = function () {
  this.column_widths = [];

  this.cells.forEach(row => {
    row.forEach((cell, colIndex) => {
      const cellLength = cell.length;
      if (typeof this.column_widths[colIndex] === 'undefined') {
        this.column_widths[colIndex] = cellLength;
      } else if (this.column_widths[colIndex] < cellLength) {
        this.column_widths[colIndex] = cellLength;
      }
    });
  });
};

////////////////////////////////////////////////////////////////////////////////

MarkdownTableFormatter.prototype.import_table = function (table) {
  const table_rows = table.split("\n").filter(row => row.includes('|'));

  table_rows.forEach((row, rowIndex) => {
    this.cells[rowIndex] = row.split("|").map(cell => cell.trim());
    if (rowIndex === 1) {
      this.cells[rowIndex] = this.cells[rowIndex].map(cell => cell.replace(/-+/g, "-"));
    }
  });

  // Remove leading and trailing rows if they are empty.
  this.get_column_widths();

  if (this.column_widths[0] === 0) {
    this.cells.forEach(row => row.shift());
  }

  this.get_column_widths();

  if (this.column_widths[this.column_widths.length - 1] === 0) {
    this.cells.forEach(row => {
      if (row.length === this.column_widths.length) {
        row.pop();
      }
    });
  }

  this.get_column_widths();
};

////////////////////////////////////////////////////////////////////////////////

MarkdownTableFormatter.prototype.pad_cells_for_output = function () {
  this.cells.forEach((row, rowIndex) => {
    row.forEach((cell, colIndex) => {
      while (this.cells[rowIndex][colIndex].length < this.column_widths[colIndex]) {
        this.cells[rowIndex][colIndex] += rowIndex === 1 ? "-" : " ";
      }
    });
  });
};
