# Markdown Table Formatter

A JavaScript tool to format markdown tables for easier reading.

[Deployed on GitLab Pages](https://mnohr.gitlab.io/markdown-table-formatter/)

## Original Version

This was forked from the original open-source GitHub repo [here](https://github.com/alanwsmith/markdown_table_formatter) and the original live site [here](https://markdowntable.com/). This version maintains the same open-source license as the original.

That version had not been updated in 10 years. This new version has the same functionality with an updated look-and-feel. I also removed any tracking code. 

Since I often want to format markdown tables that contain sensitive or confidential information, I wanted to make a copy that I could ensure was not sending or storing any information. 